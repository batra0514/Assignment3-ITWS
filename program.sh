#!/bin/bash
a=$1
b=$2
c=$3
d=$4
IFS='\n'
if [[ $a == "read" ]]
then
	if [[ $b == "eno" ]]
	then
		l=`awk -v i=$c 'FNR == i {print}' employee.txt`
		echo $l 
	fi
	if [[ $b == "ename" ]]
	then
		echo `grep -w "$c" employee.txt`  
	fi
	if [[ $2 == "esalary" ]]
	then
		echo `grep -w "$c" employee.txt`	
	fi
fi	
if [[ $a == "write" ]]
then 
	`echo "$b,$c,$d" >> employee.txt`	
	echo "DONE"
fi
if [[ $a == "delete" ]]
then
	g=`sed -i "/^$b\b/d" employee.txt`
	echo "DONE"
fi
if [[ $a == "update" ]]
then
	h=`sed -i "s/^$b.*/"$b,$c,$d"/" employee.txt`
	echo "DONE"
fi
if [[ $a == "duplicate" ]]
then
	echo `sort employee.txt | uniq --count --repeated`
fi

