#!/bin/bash
HISTFILE=~/.bash_history
set -o history
IFS=$'\n'
history | tr -s ' ' | cut -d ' ' -f 3 | sort | uniq -c | sort -rn | awk '{ print $2 " " $1}'


