#!/bin/bash
p=1
read var
a=$(echo "$var" | tr '[:upper:]' '[:lower:]')
l=`expr ${#a} - 1`
k=`expr ${#a} \/ 2 - 1`
for((i=0;i<=$k;i++))
do
	j=`expr $l - $i`
	if [ ${a:$i:1} != ${a:$j:1} ]
	then
		p=0
	fi
done
if [ $p -eq 1 -a ${#a} -ne 1 ]
then
	echo "YES"
fi
if [ $p -eq 0 -a ${#a} -ne 1 ]
then
	echo "NO"
fi
if [ ${#a} -eq 1 ]
then
	echo "NO"
fi

