#!/bin/bash
IFS=$'\n'
echo "$1" > q15_url.txt
echo "$2" >> q15_url.txt
w3m -dump $1 > fil 
w3m -dump $2 >> fil
l=`tr "[" '[\012*]' < "fil" | tr "]" '[\012*]' | tr -sc "[A-Z][a-z]" '[\012*]' | tr '[A-Z]' '[a-z]' | sort | uniq -c | sort -k1,1nr -k2,2d | awk '{print $2 " " $1}'` 
echo "$l" > $3
