#!/bin/bash
set -f
p=0
c=1

read var
read n
count=0
for((i=0;i<$n;i++))
do
	read b[count]
	((count++))

done
m=`expr ${b[0]} \* ${b[0]}`
k=`expr 2 \* ${b[0]}`
if [ $var == "+" ]
then
	for((i=0;i<$n;i++))
	do	
		p=`expr $p + ${b[$i]}`
	done
echo $p
fi

if [ $var == "*" ]
then 
	for((i=0;i<$n;i++))
	do 
		c=`expr $c \* ${b[$i]}`
	done
echo $c
fi
if [ $var == "/" ]
then
	for((i=0;i<$n;i++))
	do
		m=$(echo "scale=4;$m/${b[$i]}" | bc)
	done
echo $m
fi
if [ $var == "-" ]
then
	for((i=0;i<$n;i++))
	do
		k=`expr $k \- ${b[$i]}`
	done
echo $k
fi



